#include "../include/RootClasses.h"

bool hasColon(const std::string str) {
    return str.find(":") != std::string::npos;

}

bool hasColon(const char* c_str) {
    const std::string str(c_str);
    return str.find(":") != std::string::npos;
}

char* getHistName(const char* varexp) {
    if (hasColon(varexp)) {
        std::string v_str (varexp);
        boost::char_separator<char> sep(":");
        tokenizer tok(v_str, sep);
        std::string xy[2];
        int i = 0;
        for (std::string s : tok) {
            xy[i] = s;
            xy[i][0] = std::toupper(xy[i][0]);
            i++;
        }
        // Get names of x, y variables and combine to get hName. 
        return Form("h%s%s", xy[0].c_str(), xy[1].c_str());
    } else {
        std::string v_str (varexp);
        v_str[0] = std::toupper(v_str[0]);
        return Form("h%s", v_str.c_str());
    }
}


