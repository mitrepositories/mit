#include "../include/RootClasses.h"

// Main project directory full pathname.
const string PATH    = "/home/brandon/MIT/";
void PrintHelpMessage();

void etaPhiEnergyGrid(const char* particleType = "", const int energy = 10, const int nEvents = 1) {
    if (!strcmp(particleType, "")) { PrintHelpMessage(); return; }

    using namespace TMath;

    // ____________________________ I/O Setup. ____________________________
    const string INFILE = PATH + "rootFiles/" + Form("TD_N%d_%s_%dGeV.root", 
                                                nEvents, particleType, energy);

    cout << "Reading input from " << INFILE.substr(INFILE.find_last_of("/")) << " . . . " << endl;

    TFile* inFile   = new TFile(INFILE.data(), "READ");
    TTree* tree     = (TTree*) inFile->Get("towerTree;1");
    TH2* hGrid      = new TH2F("hGrid", "", 256, 0., 256., 124, 0., 124.);
    TH2* hValues    = new TH2F("hValues", "", 256, -Pi(), Pi(), 124, -1.1, 1.1);
    SetDrawOptions(hGrid,   noColor, "#varphi bin", "#eta bin");
    SetDrawOptions(hValues, noColor, "#varphi (rad)", "#eta");
    
    // _________________ Drawing/Pad Customization _________________
    TCanvas* cBinGrid = new TCanvas("cBinGrid", "cBinGrid", 50, 300, 800, 600); // topx, topy, w, h
    cBinGrid->cd();
    cBinGrid->SetLogz(1);
    tree->Draw("binEta:binPhi>>hGrid", "ET");
    hGrid->Draw("colz");
    TLegend* legBin = new TLegend(0.5, 0.9, 0.88, 0.95);
    legBin->SetBorderSize(0);
    if (strcmp(particleType, "EMinus") == 0) {
        legBin->AddEntry((TObject*)0, "Single electron, p_{T}^{e^{-}} = 10 GeV", "");
    } else if (strcmp(particleType, "Gamma") == 0) {
        legBin->AddEntry((TObject*)0, "Single Photon, p_{T}^{#gamma} = 10 GeV", "");
    }
    legBin->Draw();

    TCanvas* cValGrid = new TCanvas("cValGrid", "cValGrid", 100, 400, 800, 600); // topx, topy, w, h
    cValGrid->cd();
    cValGrid->SetLogz(1);
    tree->Draw("eta:phi>>hValues", "ET");
    hValues->Draw("colz");
    legBin->Draw();

    // _________________ Save canvases as pdf files. _________________
    string OUTFILE = PATH + "pdf/" + Form("EtaPhiBinGrid_N%d_%s_%dGeV.pdf", 
                                            nEvents, particleType, energy);
    cBinGrid->SaveAs(OUTFILE.data());

    OUTFILE = PATH + "pdf/" + Form("EtaPhiValGrid_N%d_%s_%dGeV.pdf", 
                                    nEvents, particleType, energy);
    cValGrid->SaveAs(OUTFILE.data());
}

void PrintHelpMessage() {
    cout << "\nUSAGE: root 'etaPhiEnergyGrid.C([particleType], [energy], [nEvents])'" << endl;
}
