#!/bin/bash

echo "Running SaveReco.sh script . . . "

if [ $# -eq 0 ]; then 
    echo "ERROR: Y U PASS NO PROCESS??"
    exit
fi

ROOTFILES_PATH="/home/brandon/MIT/rootFiles"
OUTFILE="$1.root"

if [ -f "${ROOTFILES_PATH}/${OUTFILE}" ]; then
    echo "Removing previous file before calling hadd . . . "
    rm "${ROOTFILES_PATH}/${OUTFILE}" 
fi


echo "hadd ${ROOTFILES_PATH}/${OUTFILE} ${ROOTFILES_PATH}/${OUTFILE%.*}_*.root"
hadd ${ROOTFILES_PATH}/${OUTFILE} ${ROOTFILES_PATH}/${OUTFILE%.*}_*.root
rm ${ROOTFILES_PATH}/${OUTFILE%.*}_*.root
echo "done!"

