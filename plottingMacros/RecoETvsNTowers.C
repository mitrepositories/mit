/* -------------------------------------------------------------------------- *
    Filename:       RecoVsGenEnergy.C
    Author:         Brandon McKinzie 
    Description:    
 * -------------------------------------------------------------------------- */
#include "../include/RootClasses.h"
#include "../include/TreeManager.h"
#include "../include/util.h"

using namespace TMath;

const string PATH           = "/home/brandon/MIT/rootFiles/";
const string particleName[] = {"EMinus", "Gamma", "Pi0"};
const int nParticles        = sizeof(particleName) / sizeof(string);

TTree*  tCluster[nParticles];
    
int RecoETvsNTowers() {

    TH2* hRatioVsNTowers[nParticles];  // Reco vs Gen.
    TFile*  inFile;

    // ____________________ Set up file I/O. ____________________
    string name  = "/home/brandon/MIT/rootFiles/AllProcesses.root";
    inFile = new TFile(name.c_str(), "READ");
    
    // ____________________ Create RecoVsGen-related histograms. ____________________
    for (int i = 0; i < nParticles; i++) {
        tCluster[i] = (TTree*) inFile->Get(Form("tCluster%s;1", particleName[i].c_str()));
        SetClusterTree(tCluster[i]);
        tCluster[i]->Draw(Form("recoET/genPT:nTowers>>hRatio%d", i), "seedThresh>0.07&&seedThresh<0.12", "goff");
        hRatioVsNTowers[i] = (TH2*) gDirectory->Get(Form("hRatio%d", i));
        SetDrawOptions(hRatioVsNTowers[i], noColor, 
                "Number of Towers in Cluster", 
                "Fraction of Energy Clustered");
    }


    // _____________________ RecoEnergy vs. GenPT plots. _____________________
    TCanvas* recoCanvas = new TCanvas("recoCanvas", "recoCanvas", 150, 400, 700, 500);
    recoCanvas->Divide(3, 1);

    for (int i = 1; i <= nParticles; i++) {
        recoCanvas->cd(i)->SetLogz(1);
        hRatioVsNTowers[i - 1]->Draw("colz");
    }
    gStyle->SetOptTitle(0);

    const string particleLaTeXName[] = {"e^{-}", "#gamma", "#pi^{0}"};
    const string readableName[] = {"Electron", "Photon", "Pion"};

    for (int i = 0; i < nParticles; i++) {
        recoCanvas->cd(i + 1);
        float xMax = hRatioVsNTowers[i]->GetXaxis()->GetXmax();
        GetLine(0., 1., xMax, 1.0)->Draw();

        // ___________ Draw a legend showing particle types. ___________
        TLegend* leg = new TLegend(0.5, 0.8, 0.9, 0.9);
        leg->SetHeader(Form("Single %s Events", readableName[i].c_str()));
        //leg->AddEntry((TObject*)0, "(#eta, #varphi) = (0, 0)", "");
        //leg->AddEntry(hRatioVsNTowers[i], particleLaTeXName[i].data(), "");
        leg->SetTextSize(0.05);
        leg->Draw();

    }

    return 0;
}

