CXX = g++ -std=c++11

# Libraries to include if GZIP support is enabled
ifeq (x$(ENABLEGZIP),xyes)
LIBGZIP=-L$(BOOSTLIBLOCATION) -lboost_iostreams -L$(ZLIBLOCATION) -lz
endif

# Local directory definitions.
BASEDIR =/home/brandon/MIT
ODIR 	=$(BASEDIR)/src
IDIR 	=$(BASEDIR)/include
LDIR 	=$(BASEDIR)/lib

# Required ROOT variables.
ROOTCFLAGS 	 = `root-config --cflags`
ROOTLIBS 	 = `root-config --libs`
ROOTGLIBS    = `root-config --ldflags --glibs`
ROOTCLING 	 = `rootcling`

# Header files to include. 
_DEPS 	= RootClasses.h TreeManager.h
DEPS 	= $(patsubst %, $(IDIR)/%, $(_DEPS)) # Handy way to prepend $(IDIR) to all. 

# Object files needed by each target. 
_LIB_OBJ = RootClasses.o TreeManager.o 
_SRC_OBJ = GetEnergyGrid.o
OBJ = $(patsubst %, $(ODIR)/%, $(_SRC_OBJ)) \
	  $(patsubst %, $(LDIR)/%, $(_LIB_OBJ))

# --------------------------------------------------------------------------------------
# "All .o files depend on their associated .cxx files and the header files in DEPS."
%.o: %.cxx $(DEPS)
	@# Syntax:
	@# -c 	: generate the object file
	@# -o $@ : put the output of compilation in the file named on the left side of the : 
	@# $< 	: The first item in the dependencies list (so the .cxx file here).
	$(CXX) -c -o $@ $< $(ROOTCFLAGS) $(ROOTLIBS) $(ROOTGLIBS)

GetEnergyGrid: $(OBJ) 
	@reset
	@echo "-----------------------------------------"
	@echo "Beginning compilation of $@ . . . "
	@echo "-----------------------------------------"
	$(CXX) -o $@ $^ $(ROOTCFLAGS) $(ROOTLIBS) $(ROOTGLIBS)
	@# Syntax:
	@# $@ 	: Left side of the : 
	@# $^ 	: Right side of the : 

# .PHONY prevents make from doing something with a file named 'clean'
.PHONY: clean

clean: 
	rm -f $(ODIR)/*.o $(LDIR)/*.o
# --------------------------------------------------------------------------------------
