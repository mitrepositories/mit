// ------------------------------------------------------------------------------
//                  1. Collect all seed towers.                 
// ------------------------------------------------------------------------------

// Make the list of towers above threshold (SEEDS).
std::vector<twrs> seedTwrs;

// Obtain iterator over seeds. 
RawTowerContainer::ConstRange begin_end  = towers->getTowers();
RawTowerContainer::ConstIterator itr;
for (itr = begin_end.first; itr != begin_end.second; ++itr) {
    // Unpack RawTowerContainer pair of (id, tower).
    RawTowerDefs::keytype towerid = itr->first;
    RawTower* tower = itr->second;

    if (tower->get_energy() > _min_tower_e) {
        twrs seedTwr(tower);
        // TODO: argument will be the same for all iterations in this loop right?
        seedTwr.set_maxphibin(towergeom->get_phibins());
        seedTwr.set_id(towerid);
        seedTwrs.push_back(seedTwr);
    }
}

// ------------------------------------------------------------------------------
//                  2. Cluster towers by seed. 
// ------------------------------------------------------------------------------

// Cluster phase-space values containers.
std::vector<float> energy;
std::vector<float> eta;
std::vector<float> phi;

// Multimaps store key-value pairs, following a specific order, and
// where multiple elements can have equivalent keys. 
std::multimap<int, twrs> clusteredTowers;

// First, handle case of adjacent seeds. 
PHMakeGroups(seedTwrs, clusteredTowers); 

std::multimap<int, twrs>::iterator ctitr; 
for (ctitr = clusteredTowers.begin(); ctitr != clusteredTowers.end(); ctitr++) {
    // -----------------------------------------------------------------------
    // General cluster setup / identification (?). 
    // -----------------------------------------------------------------------

    int clusterid = ctitr->first;
    // Appears that raw cluster information is actually contained within
    // the _clusters private variable, which is a container of RawClusters, 
    // and can return a given cluster by inputting an id. At first encounter here, clusters
    // are just the connected seed components constructed by PHMakeGroups.h.
    RawCluster *cluster = _clusters->getCluster(clusterid); 

    // Assuming this reached if zero-suppressed. 
    if (!cluster) {
        cluster = new RawClusterv1();
        _clusters->AddCluster(cluster);
        energy.push_back(0.0);
        eta.push_back(0.0);
        phi.push_back(0.0);
    }

    // "second" refers to second object in the <int, twrs> pair. 
    twrs tmptower = ctitr->second;

    // I'm assuming this retrieves the seed tower of the given cluster. 
    int iphi = tmptower.get_binphi();
    int ieta = tmptower.get_bineta();
    RawTower *rawtower = towers->getTower(ieta, iphi);
    if (tmptower.get_id() != (int) RawTowerDefs::encode_towerid(towers->getCalorimeterID(), ieta, iphi )) {
        cout <<__PRETTY_FUNCTION__<< " - Fatal Error - id mismatch. internal: " << tmptower.get_id()
            << ", towercontainer: " << RawTowerDefs::encode_towerid( towers->getCalorimeterID(), ieta, iphi )
            << endl;
        exit(1);
    }

    // -----------------------------------------------------------------------
    // Cluster properties manipulations on energy/phi/eta. 
    // -----------------------------------------------------------------------

    // Cluster energy is a simple sum of the constituent towers. 
    float e = rawtower->get_energy();
    energy[clusterid] += e;

    // Get numerator of energy-weighted average tower location. 
    // This will result in cluster position being near highest density of energy.
    eta[clusterid] += e * towergeom->get_etacenter(rawtower->get_bineta());
    phi[clusterid] += e * towergeom->get_phicenter(rawtower->get_binphi());

    // Not sure if there is any particular reason why the tower is added here. 
    cluster->addTower(rawtower->get_id(), rawtower->get_energy());

    if (verbosity) {
        std::cout << "RawClusterBuilder id: " << (ctitr->first) << " Tower: "
            << " (ieta,iphi) = (" << rawtower->get_bineta() << "," << rawtower->get_binphi() << ") "
            << " (eta,phi,e) = (" << towergeom->get_etacenter(rawtower->get_bineta()) << ","
            << towergeom->get_phicenter(rawtower->get_binphi()) << ","
            << rawtower->get_energy() << ")"
            << std::endl;
    }

    // Iterate over clusters and finalize associated values (?).
    unsigned int nclusters = _clusters->size();
    for (unsigned int icluster = 0; icluster < nclusters; icluster++) {
        if (energy[icluster] > 0) {
            eta[icluster] /= energy[icluster];
            phi[icluster] /= energy[icluster];
        } else {
            eta[icluster] = 0.0;
            phi[icluster] = 0.0;
        }

        if(phi[icluster] > M_PI)  phi[icluster] = phi[icluster] - 2.*M_PI; // convert [0,2pi] to [-pi,pi] for slat geometry(L. Xue)
        RawCluster *cluster = _clusters->getCluster(icluster);
        cluster->set_energy(energy[icluster]);
        cluster->set_eta(eta[icluster]);
        cluster->set_phi(phi[icluster]);

        if (verbosity) {
            cout << "RawClusterBuilder: Cluster # " << icluster << " of " << nclusters << " "
                << " (eta,phi,e) = (" << cluster->get_eta() << ", "
                << cluster->get_phi() << ","
                << cluster->get_energy() << ")"
                << endl;
        }
    }

    // Correct mean Phi calculation for clusters at Phi discontinuity
    // Assumes that Phi goes from -pi to +pi
    for (unsigned int icluster = 0; icluster < nclusters; icluster++) {
        RawCluster *cluster = _clusters->getCluster(icluster);
        float oldphi = cluster->get_phi();
        bool corr = CorrectPhi(cluster, towers,towergeom);
        if (corr && verbosity) {
            std::cout << PHWHERE << " Cluster Phi corrected: " << oldphi << " " << cluster->get_phi() << std::endl;
        }
    }

    if (chkenergyconservation) {
        double ecluster = _clusters->getTotalEdep();
        double etower = towers->getTotalEdep();
        if (ecluster > 0) {
            if (fabs(etower - ecluster) / ecluster > 1e-9) {
                cout << "energy conservation violation: ETower: " << etower
                    << " ECluster: " << ecluster 
                    << " diff: " << etower - ecluster << endl;
            }
        } else {
            if (etower != 0) {
                cout << "energy conservation violation: ETower: " << etower
                    << " ECluster: " << ecluster << endl;
            }
        }
    }
    return Fun4AllReturnCodes::EVENT_OK;
}

