#include <iostream>
#include "TH2.h"
#include "TH2F.h"
#include "TFile.h"
#include "../include/TreeManager.h"
#include "../include/PadIter.h"

const char* PATH = "/home/brandon/MIT";
const Float_t pi = TMath::Pi();

void GetEnergyGrid() {
    // Setup desired input/output ROOT files.
    TFile* outFile  = new TFile(Form("%s/rootFiles/getLetterGrid.root", PATH), "RECREATE");
    TFile* inFile   = new TFile(Form("%s/rootFiles/g4cemc_eval.root", PATH));

    // Setup the tree manager.
    std::vector<const char*> vars = {"ieta", "iphi", "e", "iphi:ieta"};
    TreeManager* treeManager = new TreeManager((TTree*) inFile->Get("ntp_tower;1"));
    treeManager->Extract(vars);

    // Draw desired quantities.
    outFile->cd();
    TCanvas* canvas = new TCanvas("canvas", "canvas", 800, 500);
    canvas->Divide(4, 1);  PadIter::Init(4);
    for (auto v : vars) {
        treeManager->Draw(v, canvas->cd(PadIter::Next()));
    }
    canvas->WaitPrimitive();
}

#ifndef __CINT__
int main(int argc, char* argv[]) {
    TApplication theAPP("App", &argc, argv);
    GetEnergyGrid();
    theAPP.Terminate();
    return 0;
}
#endif


