#ifndef ROOTCLASSES_H 
#define ROOTCLASSES_H 

// C++ header files. 
#include <iostream>
#include <initializer_list>
#include <locale>
#include <map>
#include <string>
#include <vector>
#include <utility> // std::pair, std::make_pair
#include <boost/tokenizer.hpp>
// ROOT header files.
#include "TApplication.h"
#include "TBranch.h"
#include "TCanvas.h"
#include "TClonesArray.h"
#include "TCut.h"
#include "TDirectory.h"
#include "TF1.h"
#include "TFile.h"
#include "TGenPhaseSpace.h"
#include "TGraph.h"
#include "TGraphAsymmErrors.h"
#include "TH1.h"
#include "TH1F.h"
#include "TH2.h"
#include "TH2F.h"
#include "THn.h"
#include "THStack.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TLine.h"
#include "TLorentzVector.h"
#include "TMath.h"
#include "TNtuple.h"
#include "TPad.h"
#include "TRandom.h"
#include "TRandom3.h"
#include "TROOT.h"
#include "TString.h"
#include "TStyle.h"
#include "TTree.h"

const Color_t noColor   = 0;
const Color_t myBlue    = 36;
const Color_t myGreen   = 30;
const Color_t myPurple  = 221;
const Color_t myRed     = 49;
const Color_t col[]     = {myBlue, myGreen, myPurple, myRed};
const Color_t sty[]     = {kOpenStar, kOpenDiamond, kOpenCircle, kOpenSquare};
const Color_t smallSty[]     = {kFullDotMedium, kPlus, kMultiply, kStar};

// ______________________ FUNCTION PROTOTYPES ________________________
//template<typename Hist>
//void SetDrawOptions(Hist* h, Color_t col=myBlue, const char* x_label="", const char* y_label="");

// ______________________ TEMPLATE DEFINITIONS ________________________
template<typename Hist>
void SetDrawOptions(Hist* h, Color_t col, const char* x_label="", const char* y_label="") {
    h->SetStats(0);
    // Distinctions b/w 1D and 2D hists.
    if (h->InheritsFrom(TH2::Class())) {
        h->SetDrawOption("COLZ1");
    } else {
        // Line and fill properties.
        h->SetFillColor(col);
        h->SetLineColor(col);
        h->SetLineWidth(2);
        h->SetFillStyle(0);
        // Marker Properties.
        h->SetMarkerColor(col);
        h->SetMarkerStyle(kOpenStar);
    }
    // Axis properties.
    if (strcmp(x_label, "")) h->GetXaxis()->SetTitle(x_label);
    if (strcmp(y_label, "")) h->GetYaxis()->SetTitle(y_label);
    h->GetXaxis()->SetTitleSize(0.05);
    h->GetYaxis()->SetTitleOffset(1.4);
    h->GetYaxis()->SetTitleSize(0.04);
    h->GetXaxis()->CenterTitle();
    h->GetYaxis()->CenterTitle();
}

TLine* GetLine(float x1, float y1, float x2, float y2) {
    TLine* line = new TLine(x1, y1, x2, y2);
    line->SetLineStyle(2);
    return line;
}

TLine* GetLine(float lo, float hi) {
    TLine* line = new TLine(lo, lo, hi, hi);
    line->SetLineStyle(2);
    return line;
}

#endif
