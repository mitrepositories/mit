#ifndef HISTHELPER_H
#define HISTHELPER_H

#ifndef ROOTCLASSES_H
#include "RootClasses.h"
#endif

// Cluster variables. 
std::vector<int>* towerIDs;
int iEventCluster;      // Event ID for this cluster. 
float seedThreshCluster;
int clusterID;
float genPT;
float clusterEnergy;
float clusterET;
float clusterEta;
float clusterPhi;
int nClusters;
int nTowers; // Number of towers in this cluster. 

// Tower variables. 
float iEventTower;  // Event ID for this tower.
float seedThreshTower;
float towerID;
float towerClusterID;
float towerEnergy;
float towerET;
float towerEta;
float towerPhi;
float towerBinEta;
float towerBinPhi;
float nBinsEta;
float nBinsPhi;
//float nEventTowers; // Total number of towers in this event. 


TTree* GetClusterTree(TFile* f, string particleName) {
    TTree* tCluster = (TTree*) f->Get(Form("tCluster%s;1", particleName.c_str()));
    tCluster->SetBranchAddress("towerIDs", &towerIDs);
    tCluster->SetBranchAddress("iEvent", &iEventCluster);
    tCluster->SetBranchAddress("seedThresh", &seedThreshCluster);
    tCluster->SetBranchAddress("clusterID", &clusterID);
    tCluster->SetBranchAddress("genPT", &genPT);
    tCluster->SetBranchAddress("recoEnergy", &clusterEnergy);
    tCluster->SetBranchAddress("recoET", &clusterET);
    tCluster->SetBranchAddress("eta", &clusterEta);
    tCluster->SetBranchAddress("phi", &clusterPhi);
    tCluster->SetBranchAddress("nClusters", &nClusters);
    tCluster->SetBranchAddress("nTowers", &nTowers);
    return tCluster;
}

TTree* GetTowerTree(TFile* f, string particleName) {
    TTree* tTower = (TTree*) f->Get(Form("tTower%s;1", particleName.c_str()));
    tTower->SetBranchAddress("iEvent", &iEventTower);
    tTower->SetBranchAddress("seedThresh", &seedThreshTower);
    tTower->SetBranchAddress("towerID", &towerID);
    tTower->SetBranchAddress("clusterID", &towerClusterID);
    tTower->SetBranchAddress("energy", &towerEnergy);
    tTower->SetBranchAddress("ET", &towerET);
    tTower->SetBranchAddress("eta", &towerEta);
    tTower->SetBranchAddress("phi", &towerPhi);
    tTower->SetBranchAddress("ieta", &towerBinEta);
    tTower->SetBranchAddress("iphi", &towerBinPhi);
    tTower->SetBranchAddress("nBinsEta", &nBinsEta);
    tTower->SetBranchAddress("nBinsPhi", &nBinsPhi);
    return tTower;
}

void SetClusterOut(TTree* outTree) {
    outTree->Branch("towerIDs", &*towerIDs);
    outTree->Branch("iEvent", &iEventCluster);
    outTree->Branch("seedThresh", &seedThreshCluster);
    outTree->Branch("clusterID", &clusterID);
    outTree->Branch("genPT", &genPT);
    outTree->Branch("recoEnergy", &clusterEnergy);
    outTree->Branch("recoET", &clusterET);
    outTree->Branch("eta", &clusterEta);
    outTree->Branch("phi", &clusterPhi);
    outTree->Branch("nClusters", &nClusters);
    outTree->Branch("nTowers", &nTowers);
}

void SetTowerOut(TTree* towerOut) {
    towerOut->Branch("iEvent", &iEventTower);
    towerOut->Branch("seedThresh", &seedThreshTower);
    towerOut->Branch("towerID", &towerID);
    towerOut->Branch("clusterID", &towerClusterID);
    towerOut->Branch("energy", &towerEnergy);
    towerOut->Branch("ET", &towerET);
    towerOut->Branch("eta", &towerEta);
    towerOut->Branch("phi", &towerPhi);
    towerOut->Branch("ieta", &towerBinEta);
    towerOut->Branch("iphi", &towerBinPhi);
    towerOut->Branch("nBinsEta", &nBinsEta);
    towerOut->Branch("nBinsPhi", &nBinsPhi);
}

// 2 dec places
float _round(float val) { return roundf(val * 100.) / 100.; }

// Get the unique values of seed thresh in tree t.
vector<float> GetSeedThresholds(TTree* t) {
    vector<float> res;
    t->Draw("seedThresh>>h", "", "goff");
    TH1* h = (TH1*) gDirectory->Get("h");
    // TODO: remove hardcoded skipping of 0.05 by init for loop to 10.
    for (int iBin = 10; iBin <= h->GetNbinsX(); iBin++) {
        float content = h->GetBinContent(iBin);
        if (content > 0.) res.push_back(_round(h->GetBinLowEdge(iBin)));
    }
    return res;
}


#endif
