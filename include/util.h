/* -------------------------------------------------------------- *
   util.h: Useful standalone functions.
 * -------------------------------------------------------------- */
#ifndef UTIL_H
#define UTIL_H

// Uncomment to disable assert().
// #define NDEBUG
#include <cassert>
#include <fstream>
#ifndef ROOTCLASSES_H
#include "RootClasses.h"
#endif

const float eps = 0.001;
const string ROOTDIR = "/home/brandon/MIT/rootFiles/";
enum DesiredProcess { extraClusters, singleCluster, fixedSeedSimple, fixedSeed, numTowers, allProcesses, allSimple };

void PrintFile(TFile* f) {
    string fileName = f->GetName();
    cout << "Reading input from " 
         << fileName.substr(fileName.find_last_of("/") + 1)
         << " . . . " << endl;
    cout << "File Contents:" << endl;
    f->ls();
    cout << endl;
}

string GetProcessString(DesiredProcess process) {
    string res;
    switch(process) { // alphabetized by process.
        case allProcesses:  res = "AllProcesses";   break;
        case allSimple:     res = "simple_rcb";     break;
        case extraClusters: res = "ExtraClusters";  break;
        case numTowers:     res = "SmallNTowers";   break;
        case singleCluster: res = "SingleCluster";  break;
        case fixedSeed:     res = "FixedSeed";      break;
        case fixedSeedSimple: res = "FixedSeedSimple"; break;
        default: res = "rcb";
    }
    return res;
}

TFile* GetRCBFile(string particleName, bool wantSimple=false) {
    string procStr  = wantSimple ? "simple_rcb" : "rcb";
    string fileName = ROOTDIR + "archive/" + procStr + particleName + ".root";
    TFile* file = new TFile(fileName.data(), "READ");
    PrintFile(file);
    return file;
}

// Read file built by RCBFileProcessor.
TFile* GetRCBFile(DesiredProcess process=allProcesses) {
    string procStr  = GetProcessString(process);
    string fileName = ROOTDIR + procStr + ".root";
    TFile* file = new TFile(fileName.data(), "READ");
    PrintFile(file);
    return file;
}


string GetLaTeXName(string particle) {
    string s;
    if (particle == "EMinus") {
        s = "e^{-}"; 
    } else if (particle == "Gamma") {
        s = "#gamma";
    } else if (particle == "Pi0") {
        s = "#pi^{0}"; 
    } else {
        s = ""; 
    }
    return s;
}

string GetReadableName(string particle) {
    string s;
    if (particle == "EMinus") {
        s = "Electron"; 
    } else if (particle == "Gamma") {
        s = "Photon";
    } else if (particle == "Pi0") {
        s = "Pion"; 
    } else {
        s = ""; 
    }
    return s;
}
istream& operator>>(istream& is, DesiredProcess& process) {
    int tmp;
    if (is >> tmp) process = static_cast<DesiredProcess>(tmp);
    return is;
}


// TODO: decide whether or not to keep.
string PathTo(string dirName) {
    const string HOME = "/home/brandon";
    string res;
    if (dirName == "rootFiles") res = Form("%s/rootFiles", HOME.c_str());
    else res = "error";
    return res;
}

void NormalizeHist(TH1* h) {
    cout << "Before: " << h->Integral() << endl;
    h->Scale(1.0 / h->Integral());
    cout << "After: " << h->Integral() << endl;
}
#endif
