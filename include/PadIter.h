#ifndef PADITER_H_
#define PADITER_H_
#include <iostream>

class PadIter {
    public:
        static void Init(int nPads) { i=0; n=nPads; }
        static int Next() { return ++i % (n + 1); }
        static int i;
        static int n;
};
int PadIter::i;
int PadIter::n;

#endif
