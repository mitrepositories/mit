# Shorthand Definitions
* TD: TowerDumper
* N: Number of events
* [number]GEV: Particle(s) hardcoded to all have _exactly_ pT = [number]GEV

* recoET vs genPT, nTowers < 5 cut. 
* recoET/genPT 1-dim hist.
* dotted line along mean in addition to line of unit slope. 
* 10 MeV bins.
* obtain an energy correction. constant factor?
* events with more than one cluster, where at least two have fract energy > 0.4 or something.
* also event with no photon/particle found. Les

* how often missed/ spread in energy/ 

