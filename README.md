# Overview

This repository contains ROOT macros to process and analyze the ROOT files currently output by RawClusterBuilderIA. The main
source files to produce the plots are located in the plottingMacros directory. More info will be placed here soon.

## __Fun4All\_G4\_sPHENIX.C__
* My module is called near the end by the lines:
    + TowerDumper* td = new TowerDumper();
    + se->registerSubsystem(td);
* The file can run this because we load:
    + gSystem->Load("libTowerDumper.so");
* To change the number/type of generated particles, see:
    + PHG4SimpleEventGenerator* gen = new PHG4SimpleEventGenerator();
    + gen->add\_particles("e-", 1);
